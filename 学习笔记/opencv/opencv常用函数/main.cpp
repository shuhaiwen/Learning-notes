#include<opencv2/core.hpp>
#include<opencv2/highgui.hpp>
#include<opencv2/imgproc.hpp>
#include<iostream>
//鼠标事件函数
void onMouse(int event, int x, int y, int flags, void* param) {

	cv::Mat *im = reinterpret_cast<cv::Mat*>(param);

	switch (event) {	// dispatch the event

	case cv::EVENT_LBUTTONDOWN: // mouse button down event

		// display pixel value at (x,y)
		std::cout << "at (" << x << "," << y << ") value is: "
			<< static_cast<int>(im->at<uchar>(cv::Point(x, y))) << std::endl;
		break;
	}
}
int main(int argc, char** argv)
{
	cv::Mat image,result;
	//1.图片读取
	image = cv::imread("../../images/puppy.bmp", cv::IMREAD_GRAYSCALE);
	//2.图片保存
	cv::imwrite("write.bmp", image);
	//3.图片显示
	cv::imshow("img", image);
	//4.窗口显示
	cv::namedWindow("Original Image"); 
	//5.鼠标事件回调函数
	cv::setMouseCallback("Original Image", onMouse, reinterpret_cast<void*>(&image));
	//6.翻转
	cv::flip(image, result, 1); // positive for horizontal
								// 0 for vertical,                     
								// negative for both
	//7.画圆
	cv::circle(image,              // destination image 
		cv::Point(155, 110), // center coordinate
		65,                 // radius  
		0,                  // color (here black)
		3);                 // thickness
	//8.写字
	cv::putText(image,                   // destination image
		"This is a dog.",        // text
		cv::Point(40, 200),       // text position
		cv::FONT_HERSHEY_PLAIN,  // font type
		2.0,                     // font scale
		255,                     // text color (here white)
		2);                      // text thickness
	//9.ROI
	{
		cv::Mat logo = cv::imread("smalllogo.png");

		// define image ROI at image bottom-right
		cv::Mat imageROI(image,
			cv::Rect(image.cols - logo.cols, //ROI coordinates
				image.rows - logo.rows,
				logo.cols, logo.rows));// ROI size
		//10.insert logo
		logo.copyTo(imageROI);
	}
	//11.矩阵
	cv::Matx33d matrix(3.0, 2.0, 1.0,
					   2.0, 1.0, 3.0,
					   1.0, 2.0, 3.0);
	//12.分割通道&&合并通道
	{
		std::vector<cv::Mat> planes;
		// split 1 3-channel image into 3 1-channel images
		cv::split(image, planes);
		// merge the 3 1-channel images into 1 3-channel image
		cv::merge(planes, result);
	}
	//13.重分配图片
	result.create(image.size(), image.type()); // allocate if necessary
	//14滤波器
	{
		cv::Mat kernel(3, 3, CV_32F, cv::Scalar(0));
		// assigns kernel values
		kernel.at<float>(1, 1) = 5.0;
		kernel.at<float>(0, 1) = -1.0;
		kernel.at<float>(2, 1) = -1.0;
		kernel.at<float>(1, 0) = -1.0;
		kernel.at<float>(1, 2) = -1.0;
		cv::filter2D(image, result, image.depth(), kernel);
	}
	//15色彩空间转换
	cv::cvtColor(image, result, cv::COLOR_BGR2Lab);
	//16.阀值化
	cv::threshold(image,  // input image
		result,  // output image
		125, // threshold (must be < 256)
		255,     // max value
		cv::THRESH_BINARY_INV); // thresholding type
	//17.分割算法
	{
		// define bounding rectangle 
		cv::Rect rectangle(50, 25, 210, 180);
		// the models (internally used)
		cv::Mat bgModel, fgModel;
		// segmentation result
		cv::Mat result; // segmentation (4 possible values)
		// GrabCut segmentation
		cv::grabCut(image,    // input image
			result,   // segmentation result
			rectangle,// rectangle containing foreground 
			bgModel, fgModel, // models
			5,        // number of iterations
			cv::GC_INIT_WITH_RECT); // use rectangle
		cv::compare(result, cv::GC_PR_FGD, result, cv::CMP_EQ);
		// create a white image
		cv::Mat foreground(image.size(), CV_8UC3,
			cv::Scalar(255, 255, 255));

		image.copyTo(foreground, result); // bg pixels not copied
	}
	//18.计算直方图
	{
		int histSize[3];        // size of each dimension
		float hranges[2];       // range of values (same for the 3 dimensions)
		const float* ranges[3]; // array of ranges for each dimension
		int channels[3];        // channel to be considered
		// BGR color histogram
		hranges[0] = 0.0;    // BRG range
		hranges[1] = 256.0;
		channels[0] = 0;		// the three channels 
		channels[1] = 1;
		channels[2] = 2;
		cv::Mat hist;
		// Compute histogram
		cv::calcHist(&image,
			1,			// histogram of 1 image only
			channels,	// the channel used
			cv::Mat(),	// no mask is used
			hist,		// the resulting histogram
			3,			// it is a 3D histogram
			histSize,	// number of bins
			ranges		// pixel value range
		);
	}
	//19.归一化
	void cv::normalize(
		InputArray src,
		InputOutputArray dst,
		double alpha = 1,
		double beta = 0,
		int norm_type = NORM_L2,
		int dtype = -1,
		InputArray mask = noArray()
		);

	//x.等待按键
	cv::waitKey(0); // 0 to indefinitely wait for a key pressed
					// specifying a positive value will wait for
					// the given amount of msec


}